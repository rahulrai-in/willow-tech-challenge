﻿using System;
using FluentAssertions;
using Solution3.Domain;
using Xunit;

namespace Solution3.Test.Domain
{
    public class RxJobTest
    {
        [Fact]
        public void CompleteShouldUpdateFields()
        {
            // arrange
            var sut = new RxJob
            {
                Status = "InProgress",
                DateCompleted = null,
                StatusNum = 5
            };

            // act
            sut.Complete();

            // assert
            sut.Status.Should().Be("Complete");
            sut.StatusNum.Should().Be(1);
            sut.DateCompleted.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMinutes(2));
        }
    }
}
