﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoNSubstitute;
using FluentAssertions;
using NSubstitute;
using Solution3.Application.CommandHandlers;
using Solution3.Application.Commands;
using Solution3.Domain;
using Solution3.Infrastructure.Repository;
using Xunit;

namespace Solution3.Test.Application.CommandHandler
{
    public class CompleteJobCommandHandlerTest
    {
        private readonly IFixture _fixture =
            new Fixture().Customize(new AutoNSubstituteCustomization {ConfigureMembers = true});

        private CompleteJobCommandHandler _handler;
        private readonly IJobRepository _jobRepository;
        private List<RxJob> _jobs;


        public CompleteJobCommandHandlerTest()
        {
            _jobRepository = _fixture.Freeze<IJobRepository>();
            _handler = _fixture.Create<CompleteJobCommandHandler>();
        }

        [Fact]
        public async Task HandlerUpdatesStatusOfJob()
        {
            // arrange
            var jobGuid = Guid.NewGuid();
            var command = _fixture.Create<CompleteJobCommand>();
            command.JobId = jobGuid;
            var cancellationToken = new CancellationTokenSource().Token;
            var ownership = new object();
            
            ArrangeJobs(command);
            ArrangeRepository(ownership, cancellationToken, true);

            // act
            var result = await _handler.Handle(command, cancellationToken);

            // assert
            result.Should().BeTrue();
            _jobs.First(j => j.Id == jobGuid).Status.Should().Be("Complete");
            _jobs.First(j => j.Id == jobGuid).StatusNum.Should().Be(1);
            _jobs.First(j => j.Id == jobGuid).DateCompleted.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMinutes(2));
        }

        private void ArrangeJobs(CompleteJobCommand command)
        {
            _jobs = new List<RxJob>
            {
                new RxJob()
                {
                    Name = "Job 1",
                    Id = command.JobId,
                    Status = "In Progress",
                    StatusNum = 2
                }
            };
        }

        private void ArrangeRepository(object ownership, CancellationToken cancellationToken, bool result)
        {
            _jobRepository.UnitOfWork.GetContextOwnership().Returns(ownership);
            _jobRepository.UnitOfWork.SaveEntitiesAsync(ownership, cancellationToken).Returns(result);
            _jobRepository.GetAsync(Arg.Any<Guid>()).Returns(args => Task.FromResult(_jobs.FirstOrDefault(e => e.Id == (Guid)args[0])));
        }
    }
}
