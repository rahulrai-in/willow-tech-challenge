﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Solution3.Domain;
using Solution3.Test.Fixtures;
using Xunit;

namespace Solution3.Test.Api.Controllers
{
    [Collection(TestCollections.ApiIntegration)]
    public class JobControllerIntegrationTest
    {
        private readonly HttpClient _client;
        private readonly TestServerFixture _testServerFixture;

        public JobControllerIntegrationTest(TestServerFixture testServerFixture)
        {
            _testServerFixture = testServerFixture;
            _client = testServerFixture.Client;
        }

        [Fact]
        public async Task GetSummaryShouldReturnValidResponse()
        {
            // arrange
            var roomTypeId = Guid.NewGuid();
            var jobId = Guid.NewGuid();
            _testServerFixture.ResetDatabase();
            ArrangeJobAndRoomType(roomTypeId, jobId: jobId);
            var request = GetJobSummaryRequest();

            // act
            var response = await _client.SendAsync(request);

            // assert
            await EnsureSuccessResponseWithTrace(response);
            var content = await GetResponseBody(response);
            content.Should()
                .Be($"{{\"status\":[{{\"roomType\":\"Test Room Type\",\"jobStatus\":\"Complete\",\"count\":1}}]}}");
            var jobs = LoadJobs();
            jobs.Count.Should().Be(1);
            jobs.First().Id.Should().Be(jobId);
            jobs.First().RoomTypeId.Should().Be(roomTypeId);
        }

        private void ArrangeJobAndRoomType(Guid roomTypeId = default(Guid), string roomTypeName = "Test Room Type",
            Guid jobId = default(Guid), string jobName = "Test Job", string status = "Complete", int statusId = 1)
        {
            var context = _testServerFixture.GetDbContext();
            roomTypeId = roomTypeId == Guid.Empty ? Guid.NewGuid() : roomTypeId;
            var roomType = new RxRoomType {Description = "Test Description", Id = roomTypeId, Name = roomTypeName};
            context.Add(roomType);
            jobId = jobId == Guid.Empty ? Guid.NewGuid() : jobId;
            var job = new RxJob
            {
                ContractorId = 1,
                DateCompleted = DateTime.Now,
                DateCreated = DateTime.Today,
                DateDelayed = null,
                DelayReason = string.Empty,
                Floor = 1,
                Id = jobId,
                Name = jobName,
                RoomTypeId = roomTypeId,
                Status = status,
                StatusNum = statusId
            };
            context.Add(job);
            context.SaveChanges();
        }

        private static HttpRequestMessage GetJobSummaryRequest()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/Job/summary");
            return request;
        }

        private static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

        private static async Task EnsureSuccessResponseWithTrace(HttpResponseMessage response)
        {
            var body = await GetResponseBody(response);
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                throw new Exception($"Response was not successful: {response.StatusCode}:\r\nBody was: \r\n{body}");
            }
        }

        private List<RxJob> LoadJobs()
        {
            var context = _testServerFixture.GetDbContext();
            var jobs = context.GetQuery<RxJob>();
            return jobs.ToList();
        }
    }
}