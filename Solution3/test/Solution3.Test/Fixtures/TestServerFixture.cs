﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Solution3.Api;
using Solution3.Infrastructure;

namespace Solution3.Test.Fixtures
{
    public class TestServerFixture : IDisposable
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public TestServerFixture()
        {
            _factory = new WebApplicationFactory<Startup>();
            Client = _factory.CreateClient();
            ServiceProvider = _factory.Server.Host.Services;
            Configuration = ServiceProvider.GetRequiredService<IConfiguration>();
        }

        public HttpClient Client { get; }

        public IConfiguration Configuration { get; }

        public IServiceProvider ServiceProvider { get; }

        public void Dispose()
        {
            Client.Dispose();
            _factory.Dispose();
        }

        public void ResetDatabase()
        {
            // delete all records from all tables except migration history
            var context = GetDbContext();
            var sql = "DELETE FROM RX_Job;\r\n" +
                      "DELETE FROM RX_RoomType;\r\n";

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            context.Database.ExecuteSqlCommand(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        public MaintenanceContext GetDbContext()
        {
            var configuration = ServiceProvider.GetRequiredService<IConfiguration>();
            var connectionString = configuration.GetConnectionString("Maintenance");
            var builder = new DbContextOptionsBuilder<MaintenanceContext>().UseSqlServer(connectionString);
            var context = new MaintenanceContext(builder.Options);
            return context;
        }
    }
}