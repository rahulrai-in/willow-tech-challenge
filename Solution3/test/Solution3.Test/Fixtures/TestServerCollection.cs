﻿using Xunit;

namespace Solution3.Test.Fixtures
{
    [CollectionDefinition(TestCollections.ApiIntegration)]
    public class TestServerCollection : ICollectionFixture<TestServerFixture>
    {
    }
}