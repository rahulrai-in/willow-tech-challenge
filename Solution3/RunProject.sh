#!/bin/bash

# functions start
createServer() {
    printf "\nCleaning old data...\n"
    docker container rm -f demosql

    printf "\nCreating database server...\n"
    docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=NoTaP@ssW0rd" -p 1433:1433 --name demosql -d mcr.microsoft.com/mssql/server:2017-latest

    # wait for 30 seconds to allow the container to get active
    printf "\nWaiting for database server to get ready...\n"
    sleep 30
}

launchDBMigrations() {
    printf "\nLaunch database migrations...\n"
    dotnet run --project ./src/Solution3.MigrationManager/Solution3.MigrationManager.csproj -- "Data Source=localhost,1433;Initial Catalog=demo;PersistSecurityInfo=True;User ID=sa;Password=NoTaP@ssW0rd;"
}

launchBrowser() {
    sleep 5
    # use this statement for Linux\MacOS
    #open $1

    # use this comand for windows
    start $1
}
# functions end

set +e

createServer

# restore and build
printf "\nBuilding solution in release mode...\n"
dotnet build --configuration Release

# launch database migrations
launchDBMigrations

# execute tests
printf "\nRunning tests...\n"
dotnet test

# recreate database because integration tests clear saved data
createServer

launchDBMigrations

# which project do you want to run
printf "\nEnter a choice (1 or 2):\n"
echo "1: Launch API"
echo "2: Web App"

read userChoice

if [ $userChoice -eq 1 ]; then
    ASPNETCORE_URLS=https://+:5001
    dotnet run --project ./src/Solution3.Api/Solution3.Api.csproj &
    launchBrowser "https://localhost:5001"
elif
    [ $userChoice -eq 2 ]
then
    ASPNETCORE_URLS=https://+:6001
    dotnet run --project ./src/Solution3.Web/Solution3.Web.csproj &
    launchBrowser "https://localhost:6001/"
fi

## done
printf "\nBye!\n"

set -e
