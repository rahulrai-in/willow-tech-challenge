﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Solution3.Application.Queries;
using Solution3.Application.ViewModels;

namespace Solution3.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly IMediator _mediator;

        public JobController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("summary")]
        public async Task<ActionResult<JobStatusByRoomTypeViewModel>> GetSummary()
        {
            var query = new GetJobStatusByRoomTypeQuery();
            var result = await _mediator.Send(query, CancellationToken.None);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}