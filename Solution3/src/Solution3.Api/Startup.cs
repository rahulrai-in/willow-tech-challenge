﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Solution3.Application.Registrations;
using Solution3.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Solution3.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddMvcCore();

            // bind database
            services.AddEntityFrameworkSqlServer().AddDbContext<MaintenanceContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Maintenance"), sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), null);
                    })
                    .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning));
            });

            // swagger
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info {Title = "Maintenance API", Version = "v1"}); });

            // Configure DI
            var container = new ContainerBuilder();
            container.RegisterModule(new MediatorModule());
            container.RegisterModule(new ApplicationModule());
            container.Populate(services);

            var provider = new AutofacServiceProvider(container.Build());
            return provider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Maintenance API V1");
                options.RoutePrefix = string.Empty;
                options.DocumentTitle = "Maintenance API";
                options.DocExpansion(DocExpansion.None);
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}