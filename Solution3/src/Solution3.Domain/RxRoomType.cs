﻿using System;

namespace Solution3.Domain
{
    public class RxRoomType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}