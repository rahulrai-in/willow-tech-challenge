﻿using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Solution3.Infrastructure;

namespace Solution3.MigrationManager
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MaintenanceContext>
    {
        public MaintenanceContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<MaintenanceContext>();

            var connectionString = configuration.GetConnectionString("Maintenance");

            builder.UseSqlServer(connectionString,
                optionsBuilder =>
                {
                    optionsBuilder.MigrationsAssembly(typeof(DesignTimeDbContextFactory).GetTypeInfo().Assembly
                        .GetName().Name);
                });

            return new MaintenanceContext(builder.Options);
        }
    }
}