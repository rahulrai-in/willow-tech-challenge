﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Solution3.MigrationManager
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                throw new Exception("No connection string supplied!");
            }

            var connectionString = args[0];
            PrepareDatabase(connectionString);

            Console.WriteLine(
                $"Migration started using connection string (first 15 chars):{connectionString.Substring(0, 15)}...");
        }

        private static string GetScriptFile(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = assembly.GetManifestResourceNames()
                .Single(s => s.EndsWith(fileName, StringComparison.OrdinalIgnoreCase));
            string result;

            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }

        private static void PrepareDatabase(string connectionString)
        {
            var builder = new SqlConnectionStringBuilder(connectionString);
            var database = builder.InitialCatalog;
            builder.InitialCatalog = string.Empty;

            using (var connection = new SqlConnection(builder.ToString()))
            {
                var conn = connection;
                conn.Open();

                var sql =
                    $"SELECT CAST(CASE WHEN EXISTS(SELECT * FROM sys.sysdatabases where name = '{database}') THEN 1 ELSE 0 END AS bit)";
                var command = new SqlCommand(sql, connection);

                if (!(bool) command.ExecuteScalar())
                {
                    Console.WriteLine($"Creating database '{database}'...");
                    var initScript = GetScriptFile("InitDatabase.sql").Replace("$DATABASE$", database);
                    var server = new Server(new ServerConnection(connection));
                    server.ConnectionContext.ExecuteNonQuery(initScript);
                }
            }
        }
    }
}