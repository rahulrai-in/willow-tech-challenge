﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Solution3.Application.Queries;
using Solution3.Application.ViewModels;
using Solution3.Application.ViewModels.Models;
using Solution3.Domain;
using Solution3.Infrastructure;

namespace Solution3.Application.QueryHandlers
{
    public class GetJobDetailsQueryHandler : IRequestHandler<GetJobDetailsQuery, JobStatusViewModel>
    {
        private readonly IMaintenanceContext _context;

        public GetJobDetailsQueryHandler(IMaintenanceContext context)
        {
            _context = context;
        }

        public async Task<JobStatusViewModel> Handle(GetJobDetailsQuery request, CancellationToken cancellationToken)
        {
            var query =
                from job in _context.GetQuery<RxJob>()
                join roomType in _context.GetQuery<RxRoomType>() on job.RoomTypeId equals roomType.Id
                orderby job.DateCreated
                select new JobStatus(job.Id, job.Name, job.Floor, job.Status, job.StatusNum, roomType.Name);
            var result = await query.AsNoTracking().ToArrayAsync(cancellationToken);
            return new JobStatusViewModel(result);
        }
    }
}