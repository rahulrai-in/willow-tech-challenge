﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Solution3.Application.Queries;
using Solution3.Application.ViewModels;
using Solution3.Application.ViewModels.Models;
using Solution3.Domain;
using Solution3.Infrastructure;

namespace Solution3.Application.QueryHandlers
{
    public class GetJobStatusByRoomTypeQueryHandler
        : IRequestHandler<GetJobStatusByRoomTypeQuery, JobStatusByRoomTypeViewModel>
    {
        private readonly IMaintenanceContext _context;

        public GetJobStatusByRoomTypeQueryHandler(IMaintenanceContext context)
        {
            _context = context;
        }

        public async Task<JobStatusByRoomTypeViewModel> Handle(GetJobStatusByRoomTypeQuery request,
            CancellationToken cancellationToken)
        {
            var query =
                    from job in _context.GetQuery<RxJob>()
                    join roomType in _context.GetQuery<RxRoomType>() on job.RoomTypeId equals roomType.Id
                    group job by new {roomType.Name, job.Status}
                    into jobGroups
                    orderby jobGroups.Key.Name
                    select new JobStatusByRoomType(jobGroups.Key.Name, jobGroups.Key.Status, jobGroups.Count());
            var result = await query.AsNoTracking().ToArrayAsync(cancellationToken);
            return new JobStatusByRoomTypeViewModel(result);
        }
    }
}