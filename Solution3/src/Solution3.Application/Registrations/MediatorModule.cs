﻿using System.Reflection;
using Autofac;
using MediatR;
using Solution3.Application.CommandHandlers;
using Module = Autofac.Module;

namespace Solution3.Application.Registrations
{
    public class MediatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Mediator>().As<IMediator>().InstancePerLifetimeScope();
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.TryResolve(t, out var o) ? o : null;
            }).InstancePerLifetimeScope();

            // Register all command and query handlers.
            builder.RegisterAssemblyTypes(typeof(CompleteJobCommandHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>)).InstancePerLifetimeScope();
        }
    }
}