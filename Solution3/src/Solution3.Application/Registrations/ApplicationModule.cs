﻿using Autofac;
using Solution3.Infrastructure;
using Solution3.Infrastructure.Repository;

namespace Solution3.Application.Registrations
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MaintenanceContext>().As<IMaintenanceContext>().InstancePerLifetimeScope();
            builder.RegisterType<JobRepository>().As<IJobRepository>().InstancePerLifetimeScope();
        }
    }
}