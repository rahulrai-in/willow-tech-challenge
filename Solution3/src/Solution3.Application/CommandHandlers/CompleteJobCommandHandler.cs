﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Solution3.Application.Commands;
using Solution3.Infrastructure.Repository;

namespace Solution3.Application.CommandHandlers
{
    public class CompleteJobCommandHandler : IRequestHandler<CompleteJobCommand, bool>
    {
        private readonly IJobRepository _jobRepository;

        public CompleteJobCommandHandler(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }

        public async Task<bool> Handle(CompleteJobCommand command, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var job = await _jobRepository.GetAsync(command.JobId);
            if (job == null)
            {
                return false;
            }

            job.Complete();

            var ownership = _jobRepository.UnitOfWork.GetContextOwnership();
            return await _jobRepository.UnitOfWork.SaveEntitiesAsync(ownership, cancellationToken);
        }
    }
}