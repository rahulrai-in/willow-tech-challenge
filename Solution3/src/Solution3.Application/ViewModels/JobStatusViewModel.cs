﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Solution3.Application.ViewModels.Models;

namespace Solution3.Application.ViewModels
{
    [DataContract]
    public class JobStatusViewModel : IViewModel
    {
        public JobStatusViewModel(IList<JobStatus> status)
        {
            Status = status;
        }

        [DataMember] 
        public IList<JobStatus> Status { get; }
    }
}