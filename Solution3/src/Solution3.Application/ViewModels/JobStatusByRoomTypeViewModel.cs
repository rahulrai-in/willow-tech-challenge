﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Solution3.Application.ViewModels.Models;

namespace Solution3.Application.ViewModels
{
    [DataContract]
    public class JobStatusByRoomTypeViewModel : IViewModel
    {
        public JobStatusByRoomTypeViewModel(IList<JobStatusByRoomType> status)
        {
            Status = status;
        }

        [DataMember] 
        public IList<JobStatusByRoomType> Status { get; }
    }
}