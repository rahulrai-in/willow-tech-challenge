﻿using System.Runtime.Serialization;

namespace Solution3.Application.ViewModels.Models
{
    [DataContract]
    public class JobStatusByRoomType
    {
        public JobStatusByRoomType(string roomType, string jobStatus, int count)
        {
            RoomType = roomType;
            JobStatus = jobStatus;
            Count = count;
        }

        [DataMember] 
        public string RoomType { get; }

        [DataMember] 
        public string JobStatus { get; }

        [DataMember] 
        public int Count { get; }
    }
}