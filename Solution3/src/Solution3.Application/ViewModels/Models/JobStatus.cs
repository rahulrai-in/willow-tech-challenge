﻿using System;
using System.Runtime.Serialization;

namespace Solution3.Application.ViewModels.Models
{
    [DataContract]
    public class JobStatus : IViewModel
    {
        public JobStatus(Guid jobId, string name, int? floor, string status, int? statusId,
            string roomTypeName)
        {
            JobId = jobId;
            Name = name;
            Floor = floor;
            Status = status;
            StatusId = statusId;
            RoomTypeName = roomTypeName;
        }

        [DataMember] 
        public Guid JobId { get; }

        [DataMember] 
        public string Name { get; }

        [DataMember] 
        public int? Floor { get; }

        [DataMember] 
        public string Status { get; }

        [DataMember] 
        public int? StatusId { get; }

        [DataMember] 
        public string RoomTypeName { get; }
    }
}