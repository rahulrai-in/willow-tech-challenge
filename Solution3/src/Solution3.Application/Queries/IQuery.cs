﻿using MediatR;

namespace Solution3.Application.Queries
{
    public interface IQuery<out T> : IRequest<T>
    {
    }
}