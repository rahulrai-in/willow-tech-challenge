﻿using System.Runtime.Serialization;
using Solution3.Application.ViewModels;

namespace Solution3.Application.Queries
{
    [DataContract]
    public class GetJobStatusByRoomTypeQuery : IQuery<JobStatusByRoomTypeViewModel>
    {
    }
}