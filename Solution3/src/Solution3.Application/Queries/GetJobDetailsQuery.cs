﻿using System.Runtime.Serialization;
using Solution3.Application.ViewModels;
using Solution3.Application.ViewModels.Models;

namespace Solution3.Application.Queries
{
    [DataContract]
    public class GetJobDetailsQuery : IQuery<JobStatusViewModel>
    {
    }
}