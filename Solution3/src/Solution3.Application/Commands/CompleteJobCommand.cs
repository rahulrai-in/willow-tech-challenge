﻿using System;
using System.Runtime.Serialization;
using MediatR;

namespace Solution3.Application.Commands
{
    [DataContract]
    public class CompleteJobCommand : IRequest<bool>
    {
        public CompleteJobCommand(Guid jobId)
        {
            JobId = jobId;
        }

        [DataMember]
        public Guid JobId { get; set; }
    }
}