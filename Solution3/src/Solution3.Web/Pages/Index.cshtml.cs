﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Solution3.Application.Commands;
using Solution3.Application.Queries;
using Solution3.Application.ViewModels;

namespace Solution3.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IMediator _mediator;

        public IndexModel(IMediator mediator)
        {
            _mediator = mediator;
        }

        public JobStatusViewModel JobStatus { get; set; }

        public async Task OnGetAsync()
        {
            var query = new GetJobDetailsQuery();
            JobStatus = await _mediator.Send(query, CancellationToken.None);
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid? id)
        {
            // ReSharper disable once InvertIf
            if (id.HasValue)
            {
                var command = new CompleteJobCommand(id.Value);
                await _mediator.Send(command, CancellationToken.None);
            }

            return RedirectToPage();
        }
    }
}