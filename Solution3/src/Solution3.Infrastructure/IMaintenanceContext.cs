﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Solution3.Core.SeedWork;

namespace Solution3.Infrastructure
{
    public interface IMaintenanceContext : IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IQueryable<T> GetQuery<T>()
            where T : class;
    }
}