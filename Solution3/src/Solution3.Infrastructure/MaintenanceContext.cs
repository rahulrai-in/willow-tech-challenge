﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Solution3.Infrastructure.EntityTypeConfiguration;

namespace Solution3.Infrastructure
{
    public class MaintenanceContext : DbContext, IMaintenanceContext
    {
        private readonly ILoggerFactory _loggerFactory;
        private object _contextOwnership;

        public MaintenanceContext(DbContextOptions options)
            : base(options)
        {
        }

        public MaintenanceContext(DbContextOptions options, ILoggerFactory loggerFactory)
            : base(options)
        {
            _loggerFactory = loggerFactory;
        }

        public object GetContextOwnership()
        {
            if (_contextOwnership != null)
            {
                return null;
            }

            _contextOwnership = new {type = "owner"};
            return _contextOwnership;
        }

        public IQueryable<T> GetQuery<T>()
            where T : class
        {
            return Set<T>();
        }

        public async Task<bool> SaveEntitiesAsync(object ownershipToken = null,
            CancellationToken cancellationToken = default)
        {
            if (ownershipToken == null || !ownershipToken.Equals(_contextOwnership))
            {
                return false;
            }

            _contextOwnership = null;
            var result = await SaveChangesAsync(cancellationToken);
            return result > -1;
        }

        public IEnumerable<EntityEntry<T>> GetEntriesForEntity<T>()
            where T : class
        {
            return ChangeTracker.Entries<T>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseLoggerFactory(_loggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(RxJobConfiguration).Assembly);
        }
    }
}