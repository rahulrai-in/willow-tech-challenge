﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Solution3.Core.SeedWork;
using Solution3.Domain;

namespace Solution3.Infrastructure.Repository
{
    public class JobRepository : IJobRepository
    {
        private readonly IMaintenanceContext _context;

        public JobRepository(IMaintenanceContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<RxJob> GetAsync(Guid commandJobId)
        {
           return await _context.GetQuery<RxJob>().FirstOrDefaultAsync(job => job.Id == commandJobId);
        }
    }
}