﻿using System;
using System.Threading.Tasks;
using Solution3.Core.SeedWork;
using Solution3.Domain;

namespace Solution3.Infrastructure.Repository
{
    public interface IJobRepository : IRepository
    {
        Task<RxJob> GetAsync(Guid commandJobId);
    }
}