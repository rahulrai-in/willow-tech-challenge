﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Solution3.Core.SeedWork
{
    public interface IUnitOfWork : IDisposable
    {
        object GetContextOwnership();

        Task<bool> SaveEntitiesAsync(object ownershipToken = null,
            CancellationToken cancellationToken = default(CancellationToken));
    }
}