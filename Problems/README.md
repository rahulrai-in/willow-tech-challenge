# Q1 – Closures
In `closures.js`, fix the bugs in the registerHandlers function.

An alert should display anchor's zero-based index within a document. 
For example: in the document below, the alert should display "2" when Google anchor is clicked since it is the third anchor element in the document and its zero-based index is 2.

```
<body>
    In my life, I used the following web search engines:<br/>
    <a href="//www.yahoo.com">Yahoo!</a><br/>
    <a href="//www.altavista.com">AltaVista</a><br/>
    <a href="//www.google.com">Google</a><br/>
</body>
```

# Q2 – SQL Schema
In `sqlschema.sql`, the users_roles table should contain the mapping between each user and their roles.

Each user can have many roles, and each role can have many users. 
Improve the provided create table statement so that:

- Only users from the users table can exist within users_roles.
- Only roles from the roles table can exist within users_roles.
- A user can only have a specific role once.

# Q3 – Website

In this task you will need to create a simple Website and Web API. You will not be marked on the presentation of your website. Please add any commands need to run the website in a readme.md. Please ensure it runs from a fresh repository pull on someone else's computer with the instructions provided in the readme.md.

Data required for this challenge can be found in the attached SQL script file. You are free to use an ORM but it is not a requirement. Please include `unit tests` as you see fit.

* [ ]  Create a database called `Demo` and run the attached script to populate and create two tables.
       An example connection string for your application. 
       (YMMV) @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Demo;Integrated Security=True"

* [ ]  Create a web page that displays all jobs found in the table `RX_Job`. Information to be displayed JobID, Name, Floor, job status, and Name of the RoomType. Rows should be highlighted based on the job status (any color is fine.The row should have a button the enables the user to mark the row as complete, you can only mark a job as complete if the status is either in progress or delayed. This action should update the database.We will not be judging the style of the webpage so please don't spend any time on the presentation of the page. What we will be looking at is your C# code, your general code architecture, and how you solved the problem.

* [ ]  Add an Web API endpoint that returns a summary of the jobs status per room type. You should return the room type name, the job status, and the total number of job status per room type. For this question just create a API endpoint, do not create an web page. 
       **Hint**: 
      Complete 1b 18 
      Delayed 1b 10

**Example wireframe:**

Check the example progress file for hints 

Progress_By_roomType.PNG 







