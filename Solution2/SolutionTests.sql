-- case: Unknown user can't be added to user_roles
INSERT INTO users_roles VALUES(4, 3)



-- case: Unknown user role can't be added to user_roles
-- arrange
INSERT INTO users VALUES(4, 'Rahul Rai')

-- act
INSERT INTO users_roles VALUES(4, 3)

-- cleanup
DELETE FROM users WHERE id=4



-- case: User can't be mapped to duplicate roles
INSERT INTO users_roles VALUES(1,2)