ALTER TABLE users_roles
ADD CONSTRAINT FK_users_roles_users
FOREIGN KEY (userid) REFERENCES users(id);

ALTER TABLE users_roles
ADD CONSTRAINT FK_users_roles_roles
FOREIGN KEY (roleid) REFERENCES roles(id);

ALTER TABLE users_roles
ADD CONSTRAINT UNQ_users_roles 
UNIQUE (userId,roleId);